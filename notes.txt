database - organized collection of info/data
	data - raw
	info - group of organized data with logical meaning

database - information stored in a computer system
		- physical databases (ex. library room)
		- managed by DBMS(database management system)
				- DBMS follows Dewey Decimal System
			- store
			- retrieve
			- modify

			terms: (CRUD Operations)
			- create(insert)
			- read(select)
			- update
			- delete

	relational database - data is stored as a set of table(with rows and columns)

	unstructured data - NoSQL database are commonly used


sql - structure query language
	- typically used for relational database

nosql - Not only SQL
	- flexible

MongoDB - open-source database, leading NoSQL database
Mongo - humongous - huge or enormous

	Terms:
	relational to MongoDB(NoSQL)
	tables = collections
	rows = documents
	columns = fields